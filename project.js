function setup() {

var canvas = document.querySelector("canvas");
var canX, canY;
var ctx = document.getElementById("globalCanvas").getContext("2d");
var i = 0;
var lineStage = 0;
var lineStartX, lineStartY, lineEndX, lineEndY, lineX, lineY;
var lineLength, circleCount, circleProjX, circleProjY;
var circleColour, circleX, circleY, currentCircleX, currentCircleY;

function resizeWindow() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}

resizeWindow();

window.addEventListener(
  "mousemove",
  function(e) {
    canX = e.x;
    canY = e.y;
  },
  { passive: true }
);

window.addEventListener("click", drawLine);

function drawCircle(circleX, circleY, circleColour) {
  ctx.strokeStyle = circleColour;
  ctx.beginPath();
  ctx.arc(circleX, circleY, 10, 0, 2 * Math.PI);
  ctx.stroke();
}

function fillTheLineWithCircles(lineStartX, lineStartY, lineEndX, lineEndY) {
  var currentCircleCount = 0;
  lineX = lineEndX - lineStartX;
  lineY = lineEndY - lineStartY;
  lineLength = Math.sqrt(Math.pow(lineX,2) + Math.pow(lineY,2));
  circleCount = Math.floor((lineLength - 20) / 20);
  circleProjX = lineX / (circleCount + 1);
  circleProjY = lineY / (circleCount + 1);
  console.log (circleCount);
  if (circleCount >= 1) {
    currentCircleX = lineStartX + circleProjX;
    currentCircleY = lineStartY + circleProjY;
    drawCircle(currentCircleX, currentCircleY, "black");
    currentCircleCount++;
    while (currentCircleCount < circleCount) {
      currentCircleX += circleProjX;
      currentCircleY += circleProjY;
      drawCircle(currentCircleX, currentCircleY, "black");
      currentCircleCount++;
    }
  } else {
    return;
  }
  ctx.fillText("Line Length = X:" + lineX + " ,Y:" + lineY + ", total: " + lineLength, 10, 90);
  ctx.fillText("Circle Count is " + circleCount, 10, 110);
}

function drawLine() {
  switch (lineStage) {
    case 0:
      drawCircle(canX, canY, "red");
      lineStartX = canX;
      lineStartY = canY;
      ctx.font = "15px monospace";
      ctx.fillText("A: " + lineStartX + " : " + lineStartY, 10, 50);
      lineStage++;
      break;
    case 1:
      drawCircle(canX, canY, "green");
      lineEndX = canX;
      lineEndY = canY;
      ctx.fillText("B: " + lineEndX + " : " + lineEndY, 10, 70);
      ctx.strokeStyle = "black";
      ctx.beginPath();
      ctx.moveTo(lineStartX, lineStartY);
      ctx.lineTo(lineEndX, lineEndY);
      ctx.stroke();
      lineStage++;
      break;
    case 2:
      fillTheLineWithCircles(lineStartX, lineStartY, lineEndX, lineEndY);
      lineStage++;
      break;
    case 3:
      resizeWindow();
      lineStage = 0;
      break;
  }
}
}

setup();